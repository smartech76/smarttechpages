<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 26.06.2018
 * Time: 10:30
 */
function enqueue_styles()
{
    wp_register_style('slick', get_template_directory_uri() . '/css/slick.css');
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css');
    wp_register_style('header', get_template_directory_uri() . '/css/header.css');
    wp_register_style('main', get_template_directory_uri() . '/css/main.css');
    wp_register_style('sliders', get_template_directory_uri() . '/css/sliders.css');
    wp_register_style('content-blocks', get_template_directory_uri() . '/css/content-blocks.css');
    wp_register_style('custom-checkbox', get_template_directory_uri() . '/css/custom-checkbox.css');

    wp_enqueue_style('robot-regular', get_stylesheet_uri());


    wp_enqueue_style('bootstrap');
    wp_enqueue_style('slick');
//        wp_enqueue_style('bootstrap-theme');
    wp_enqueue_style('header');
    wp_enqueue_style('main');
    wp_enqueue_style('sliders');
    wp_enqueue_style('content-blocks');
    wp_enqueue_style('custom-checkbox');
    wp_enqueue_style('robot-regular');

}

add_action('wp_enqueue_scripts', 'enqueue_styles');

function enqueue_scripts()
{


    wp_register_script('jquery-3.3.1', get_template_directory_uri() . '/js/jquery-3.3.1/jquery-3.3.1.min.js');
    wp_register_script('slick', get_template_directory_uri() . '/js/slick/slick.min.js');
    wp_register_script('parallax', get_template_directory_uri() . '/js/parallax.js');
    wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap/bootstrap.min.js');
    wp_register_script('sliders', get_template_directory_uri() . '/js/sliders.js');
    wp_register_script('widgets', get_template_directory_uri() . '/js/widgets.js');
    wp_register_script('image-tiles', get_template_directory_uri() . '/js/image-tiles.js');


    wp_enqueue_script('jquery-3.3.1');
    wp_enqueue_script('slick');
    wp_enqueue_script('parallax');
    wp_enqueue_script('bootstrap-js');
    wp_enqueue_script('sliders');
    wp_enqueue_script('widgets');
    wp_enqueue_script('image-tiles');

}

add_action('wp_enqueue_scripts', 'enqueue_scripts');

if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}