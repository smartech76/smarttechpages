$(document).ready(function () {

    $(window).bind('scroll', function (e) {
        parallaxScroll();
    });

    function parallaxScroll() {
        
        //Page height with scrolling
        var clientHeight = document.documentElement.clientHeight;
        //Page scrolling
        var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        //Page width with scrolling
        var clientWidth = document.documentElement.clientWidth;
        
        var yStartParalaxRequestCall = 600;
        
        if (scrollTop > clientHeight) {
            $('#container-request-call-parallax').css('z-index', 15);
        } else {
            $('#container-request-call-parallax').css('z-index', 5);
        }
        
        var scrolled = $(window).scrollTop();
        $('#container-sliders-parallax').css('top', (0 - (scrolled * .25)) + 'px');
        $('#container-request-call-parallax').css('top', (yStartParalaxRequestCall - (scrolled * .25)) + 'px');
    }

});