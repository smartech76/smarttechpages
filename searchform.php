<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 26.06.2018
 * Time: 19:09
 */?>
<form name="search" action="<?php echo home_url( '/' ) ?>" method="get" class="search-form">
    <input type="text" class="dark-input default-input header-search-widget-input"  value="<?php echo get_search_query() ?>" name="s" placeholder="<?php echo __('Найти', 'whitesquare'); ?>" >
    <button type="submit" class="btn-green btn-rectangle"><?php echo __('Найти', 'whitesquare'); ?></button>
</form>
