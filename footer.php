<?php
/**
 * Created by PhpStorm.
 * User: Пользователь
 * Date: 26.06.2018
 * Time: 10:33
 */ ?>
</div>
<footer>
    <div class="container-content content-background-dark-blue">
        <div class="container container-footer-feedback">
<!--            <div class="row">-->
<!--                <div class="col-sm-12 col-md-12 margin-middle-bottom">-->
<!--                    <h2 class="content-header-middle text-white">Отправить заявку на консультацию</h2>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col-xs-12 col-sm-6 col-md-6 footer-feedback-field">-->
<!--                    <input type="text" class="dark-input default-input pull-right" placeholder="Ваше имя">-->
<!--                </div>-->
<!--                <div class="col-xs-12 col-sm-6 col-md-6 footer-feedback-field">-->
<!--                    <input type="text" class="dark-input default-input pull-left" placeholder="Телефон">-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col-sm-12 col-md-12 margin-middle-bottom margin-middle-top">-->
<!--                    <input type="checkbox" id="footer-feedback" class="dark-checkbox custom-checkbox">-->
<!--                    <label for="footer-feedback" class="text-white">Отправляя заявку, Вы соглашаетесь на обработку Ваших-->
<!--                        персональных данных</label>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col-sm-12 col-md-12">-->
<!--                    <button class="btn-red btn-rectangle">Отправить</button>-->
<!--                </div>-->
<!--            </div>-->
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <?php echo do_shortcode('[contact-form-7 id="16" title="Форма обратной связи"]'); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container-footer-map content-background-dark-gray">
        <div class="container-map-contact">
            <div class="footer-menu-title">
                Контакты
            </div>
            <div class="footer-menu-contact">
                <div class="footer-contact-icon">
                    <span class="glyphicon glyphicon-map-marker"></span>
                </div>
                <div class="footer-contact-text">
                    <span>Ярославль, пр-кт Фрунзе, дом 3, офис 312</span>
                </div>
            </div>
            <div class="footer-menu-contact">
                <div class="footer-contact-icon">
                    <span class="glyphicon glyphicon-earphone"></span>
                </div>
                <div class="footer-contact-text">
                    <span>88-99-55</span>
                </div>
            </div>
            <div class="footer-menu-contact">
                <div class="footer-contact-icon">
                    <span class="glyphicon glyphicon-envelope"></span>
                </div>
                <div class="footer-contact-text">
                    <span>info@smart.ru</span>
                </div>
            </div>
        </div>
        <div id="footer-map">
            <img src="<? echo get_template_directory_uri() . '/images/map.png' ?>">
        </div>
    </div>

    <div class="container-content content-background-dark-gray">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="footer-menu-title">
                        Проекты
                    </div>
                    <ul class="footer-menu-list">
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="footer-menu-title">
                        Проекты
                    </div>
                    <ul class="footer-menu-list">
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="footer-menu-title">
                        Проекты
                    </div>
                    <ul class="footer-menu-list">
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                        <li>Проекты</li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-3">
                    <div class="footer-menu-title">
                        Контакты
                    </div>
                    <div class="footer-menu-contact">
                        <div class="footer-contact-icon">
                            <span class="glyphicon glyphicon-map-marker"></span>
                        </div>
                        <div class="footer-contact-text">
                            <span>Ярославль, пр-кт Фрунзе, дом 3, оффис 312</span>
                        </div>
                    </div>
                    <div class="footer-menu-contact">
                        <div class="footer-contact-icon">
                            <span class="glyphicon glyphicon-earphone"></span>
                        </div>
                        <div class="footer-contact-text">
                            <span>88-99-55</span>
                        </div>
                    </div>
                    <div class="footer-menu-contact">
                        <div class="footer-contact-icon">
                            <span class="glyphicon glyphicon-envelope"></span>
                        </div>
                        <div class="footer-contact-text">
                            <span>info@smart.ru</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-content content-background-darkness">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <span class="text-white">Сделано на WP с любовью. Все права защищены полицией нравов</span>
                </div>
            </div>
        </div>
    </div>

</footer>
<?php wp_footer(); ?>
</body>
</html>
